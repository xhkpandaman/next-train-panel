async function getInfo() {
    const res = await fetch('https://rt.data.gov.hk/v1/transport/mtr/getSchedule.php?line=AEL&sta=HOK')
    const json = await res.json()
    console.log(json);
    const STA_NAME={
        AWE:"博覽館",
        HOK:"香港"
    }
    const info = json.data['AEL-HOK'].UP
    const currTime=json.curr_time;

    document.getElementById('message').innerHTML =
        `<div>${STA_NAME[info[0].dest]} ${info[0].plat} ${getTimeDiff(currTime,info[0].time)}</div>
    <div>${STA_NAME[info[1].dest]} ${info[1].plat} ${getTimeDiff(currTime,info[1].time)}</div>`
}

function getTimeDiff(curr,target){
    const currTimestamp=new Date(curr).getTime();
    const targetTimestamp=new Date(target).getTime();
    const timeLeft=Math.ceil((targetTimestamp-currTimestamp)/1000/60);
    if(timeLeft===1){
        return "即將到達"
    }else if(timeLeft<=0){
        return "即將離開"
    }else{
        return `${timeLeft}分鐘`
    }
}

getInfo();