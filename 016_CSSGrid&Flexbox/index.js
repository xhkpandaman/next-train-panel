async function getInfo() {
    const res = await fetch('https://rt.data.gov.hk/v1/transport/mtr/getSchedule.php?line=AEL&sta=HOK')
    const json = await res.json()
    console.log(json);
    const STA_NAME={
        AWE:"博覽館",
        HOK:"香港"
    }
    const info = json.data['AEL-HOK'].UP
    const currTime=json.curr_time;
    const time=new Date(currTime).toLocaleString(undefined,{
        hour:'2-digit',
        minute:'2-digit'
    })
    let result=`<div id="time"><div>香港</div> <div>${time}</div></div>`;
    for(let trainData of info){
        const isEvenNumber=trainData.seq%2===0;
        let css="";
        if(isEvenNumber){
            css="white-box blue-bg"
        }else{
            css="white-box"
        }
        result+=`<div class="${css}">
        <div class="destination">${STA_NAME[trainData.dest]}</div> 
        <div class="platform">${trainData.plat}</div> 
        <div class="minute">${getTimeDiff(currTime,trainData.time)}</div>
        </div>`
    }
    //第二種做法：用 for loop in 的方法
    // for(let index in info){
    //     const isEvenNumber=index%2===0;
    //     let css="";
    //     if(isEvenNumber){
    //         css="white-box"
    //     }else{
    //         css="white-box blue-bg"
    //     }
    //     result+=`<div class="${css}">
    //     <div>${STA_NAME[info[index].dest]}</div> 
    //     <div class="platform">${info[index].plat}</div> 
    //     <div>${getTimeDiff(currTime,info[index].time)}</div>
    //     </div>`
    // }
    document.getElementById('message').innerHTML =result;
}

function getTimeDiff(curr,target){
    const currTimestamp=new Date(curr).getTime();
    const targetTimestamp=new Date(target).getTime();
    const timeLeft=Math.ceil((targetTimestamp-currTimestamp)/1000/60);
    if(timeLeft===1){
        return "即將到達"
    }else if(timeLeft<=0){
        return "即將離開"
    }else{
        return `${timeLeft}<span class="unit">分鐘</span>`
    }
}

getInfo();